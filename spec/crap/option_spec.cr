require "../spec_helper"

describe Crap::Option do
  it "gets a sensible action which sets its value" do
    Crap::Option(Bool).new(false).set(true).should eq true
    Crap::Option(UInt32).new(0).set(1).should eq 1
    Crap::Option(Symbol).new(:x).set(:y).should eq :y
    Crap::Option(String).new("").set("x").should eq "x"
  end

  describe "#short" do
    it "can be assigned in a block" do
      Crap::Option(Nil).new { short 'x' }.short.should eq 'x'
    end
    it "can be assigned in a constructor" do
      Crap::Option(Nil).new(short: 'x').short.should eq 'x'
    end
  end

  describe "#long" do
    it "can be assigned in a block" do
      Crap::Option(Nil).new { long "x" }.long.should eq "x"
    end
    it "can be assigned in a constructor" do
      Crap::Option(Nil).new(long: "xxx").long.should eq "xxx"
    end
  end

  describe "#summary" do
    it "can be assigned in a block" do
      Crap::Option(Nil).new { summary "foo" }.summary.should eq "foo"
    end
    it "can be assigned in a constructor" do
      Crap::Option(Nil).new(summary: "foo").summary.should eq "foo"
    end
  end

  describe "#description" do
    it "can be assigned in a block" do
      Crap::Option(Nil).new { description "bar" }.description.should eq "bar"
    end
    it "can be assigned in a constructor" do
      Crap::Option(Nil).new(description: "bar").description.should eq "bar"
    end
  end

  describe "#valid?" do
    {
      { nil, nil, nil, false },
      { nil, nil, "x", false },
      { nil, "x", nil, false },
      { nil, "x", "x", true },
      { 'x', nil, nil, false },
      { 'x', nil, "x", true },
      { 'x', "x", nil, false },
      { 'x', "x", "x", true },
    }.each do |s, l, x, valid|
      o = Crap::Option(Nil).new(short: s, long: l, summary: x)
      it "is #{valid} when short=#{s.inspect}, long=#{l.inspect} and summary=#{x.inspect}" do
        o.valid?.should eq valid
      end
    end
  end

end
