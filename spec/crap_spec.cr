require "./spec_helper"

describe Crap do
  describe ".stdin" do
    it "defaults to STDIN" { Crap.stdin.should eq(STDIN) }
  end

  describe "#stdin" do
    it "defaults to Crap.stdin" { Crap.new.stdin.should eq(Crap.stdin) }
  end

  describe ".stdout" do
    it "defaults to STDOUT" { Crap.stdout.should eq(STDOUT) }
  end

  describe "#stdout" do
    it "defaults to Crap.stdout" { Crap.new.stdout.should eq(Crap.stdout) }
  end

  describe ".stderr" do
    it "defaults to STDERR" { Crap.stderr.should eq(STDERR) }
  end

  describe "#stderr" do
    it "defaults to Crap.stderr" { Crap.new.stderr.should eq(Crap.stderr) }
  end

  describe "#name" do
    it "defaults to PROGRAM_NAME" { Crap.new.name.should eq(PROGRAM_NAME) }
    it "can be set with an arg" { Crap.new("y").name.should eq("y") }
    it "can be set with a named arg" { Crap.new(name: "y").name.should eq("y") }
    it "can be set in the block" { Crap.new { name "y" }.name.should eq("y") }
  end

  describe "#version" do
    it "can be set with by the second arg" do
      c = Crap.new("something", "13")
      c.version.should eq "13"
      c.options.map(&.long).should contain("version")
    end
    it "can be set with a named arg" do
      c = Crap.new(version: "13")
      c.version.should eq "13"
      c.options.map(&.long).should contain("version")
    end
    it "can be set with the block" do
      c = Crap.new { version "13" }
      c.version.should eq "13"
      c.options.map(&.long).should contain("version")
    end
  end

  describe "#banner" do
    it "gets a sensible default"  do
      Crap.new.banner.should eq "Usage: #{PROGRAM_NAME}"
    end
    it "can be set in the block" do
      Crap.new { banner "blah" }.banner.should eq "blah"
    end
    it "is used by parser.to_s after #setup" do
      Crap.new { setup }.parser.to_s.lines.first.should eq Crap.new.banner
    end
  end

  describe "#parser" do
    it "gets an option parser" do
      Crap.new.parser.should be_a OptionParser
    end
    it "can be set in the block" do
      op = OptionParser.new
      Crap.new { parser op }.parser.should be op
    end
  end

  describe "#setup" do
    it "adds #banner to the #parser" do
      c = Crap.new
      c.banner "applied"
      c.setup
      op = OptionParser.new { |o| o.banner = "applied" }
      c.parser.to_s.should eq op.to_s
    end

    it "adds all #commands to the #parser" do
      c = Crap.new { banner = "parent" }
      c.commands << Crap.new("child1") { summary "first subcommand" }
      c.commands << Crap.new("child2") { summary "second subcommand" }
      op = OptionParser.new do |o|
        o.banner = c.banner
        o.separator "Commands:"
        o.on("child1", "first subcommand") {}
        o.on("child2", "second subcommand") {}
      end
      c.setup
      c.parser.to_s.should eq op.to_s
    end

    it "does nothing if @setup is true" do
      c = Crap.new { setup(true) }
      c.banner "ignored"
      c.setup # should not change the parser
      c.parser.to_s.should eq ""
    end

    it "does something if @setup is false" do
      c = Crap.new { setup(false) }
      c.banner "applied"
      c.setup # should change the parser
      c.parser.to_s.should eq "applied\n"
    end
  end

  describe "#action" do
    it "gets an action" do
      Crap.new.action.should be_a Crap::Action
    end
    it "can be set in the block to a simple proc" do
      Crap.new { action {} }.action.should be_a Crap::Action
    end
    it "can be set in the block to an proc which takes args" do
      Crap.new { action { |x| } }.action.should be_a Crap::Action
    end
    it "can be set in the block to an proc which takes args and xargs" do
      Crap.new { action { |x, y| } }.action.should be_a Crap::Action
    end
  end

  describe "#parse" do
    r : Array(Array(String))? = nil
    c = Crap.new { setup }
    it "calls #parser.parse(args)" do
      c.parser.unknown_args { |a, b| r = [a, b] }
      c.parse %w(a b c -- d e)
      r.should eq [%w(a b c), %w(d e)]
    end
    it "passes ARGV by default" do
      c.parser.unknown_args { |a, b| r = [a, b] }
      c.parse
      r.should eq [ARGV, %w()]
    end
  end

  describe "#add_help_option!" do
    it "adds a help option" do
      c = Crap.new("x") { add_help_option!; setup }
      c.parser.to_s.should eq(OptionParser.new do |op|
        op.banner = "Usage: x [options...]"
        op.separator "Options:"
        op.on("-h", "--help", "print help and exit") {}
      end.to_s)
    end
  end

  describe ".parse" do
    it "works like Crap.new.parse" do
      called = false
      Crap.parse { action { called = true } }
      called.should be_true
    end
  end

  describe "a more complex example" do
    it "works" do
      bout, berr = String::Builder.new, String::Builder.new
      command = Crap.new("command") do
        summary "SUMMARY OF COMMAND"
        description "DESCRIPTION OF COMMAND"
        add_help_option!
        action { |x| bout.puts "COMMAND(#{x.join(",")})" }
        option(Bool, false, 'u', "uppercase", "uppercase everything")
        command("subcommand1", "print subcommand 1 args") do
          action { |x| bout.print "SUBCOMMAND1(#{x.join(",")})" }
        end
        command("subcommand2", "print subcommand 2 args") do
          action { |x| bout.print "SUBCOMMAND2(#{x.join(",")})" }
        end
      end
      command.parse %w(a b c)
      command.parse %w(subcommand1 x y z)
      bout.to_s.should eq "COMMAND(a,b,c)\nSUBCOMMAND1(x,y,z)"
    end
  end
end
