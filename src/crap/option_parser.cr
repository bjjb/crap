require "option_parser"

# The Crap::Mixin provides
#
# - [✓] command autocompletion
# - [ ] command argument autocompletion
# - [✓] option name autocompletion
# - [ ] option value autocompletion
# - [ ] programmable completion results
# - [ ] a default KV store for flag values
# - [ ] nice usage messages
#
# It makes most sense when mixed into an OptionParser or something like that.
class Crap
  module Mixin
    VERSION = "0.1.0"

    protected def complete(compline : String, io : IO)
      line = compline.split(/\s+/)
      name = line.shift # TODO check whether we can use this
      word = line.pop
      line.each do |flag|
        h = @handlers[word]
        @handlers.clear unless flag.starts_with?("-")
        h.block.try { |h| h.call("") }
      end
      @handlers.keys.each { |k| io.puts(k) if k.starts_with?(word) }
    end
  end

  class OptionParser
    include Mixin

    property stdout : IO = STDOUT

    def self.parse(args = ARGV) : self
      op = new
      yield op
      if ENV.has_key?("COMP_LINE")
        op.complete(ENV["COMP_LINE"], op.stdout)
        return op
      end
      op.parse(args)
      return op
    end
  end
end
