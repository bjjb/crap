class Crap
  # An Option contains data used to build up the switches in an OptionParser,
  # to provide documentation, and can act as a store for a simple value.
  class Option(T)
    # The short flag for the option
    getter short : Char?
    # The long flag for the option
    getter long : String?
    # A summary to show for the option in help. Defaults to an empty string.
    getter summary : String?
    # A longer description of the option. If present (on _any_ option), the
    # help format will be more verbose, providing multiple lines per option.
    getter description : String?
    # An action to take as soon as the option has been parsed. Default is to
    # simply set the value.
    getter action do
      ->(v : T) { set(v) }
    end

    # Builds a new Option(T) and yields it to the block.
    def self.new(*args)
      i = new(*args)
      with i yield
      i
    end

    def initialize
    end

    def initialize(@value : T?)
    end

    def initialize(@short : Char)
    end

    # Creates a new Option(T) with long and short flags
    def initialize(
      @value : T? = nil,
      @short : Char? = nil,
      @long : String? = nil,
      @summary : String? = nil,
      @description : String? = nil,
    )
    end

    def valid? : Bool
      !!(@summary && (@short || @long))
    end

    # Allows an Option(T) to be compared by its value
    delegate :==, to: @value

    # Prints an Option(T) as its value's to_s
    delegate :to_s, to: @value

    # Sets the short character
    def short(@short)
    end

    # Sets the long option name
    def long(@long)
    end

    # Sets the summary
    def summary(@summary)
    end

    # Sets the description
    def description(@description)
    end

    # Sets the action to perform for the option
    def action(&@action : T ->)
    end

    # Sets the value of the option.
    def set(@value)
    end

    # Applies the option to an OptionParser
    def apply(op : OptionParser)
      if long && short
        op.on("-#{short}", "--#{long}", summary.to_s) {}
      elsif short
        op.on("-#{short}", summary.to_s) {}
      elsif long
        op.on("--#{long}", summary.to_s) {}
      end
    end
  end
end
