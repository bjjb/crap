require "option_parser"
require "./crap/command"
require "./crap/option"
require "./crap/errors"

# A Crap is a command (or a sub-command) which can parse a command-line, using
# an auto-generated OptionParser, and contisionally perform an action. It can
# be used to create arbitrarily complex command-line applications. For example,
# here's a clone of `echo` in a single file:
#
# ```crystal
# require "crap"
# Crap.parse("echo") do
#   nonewline = true
#   option(Bool, false, 'n', "do not append a newline")
#   summary "Write arguments to the standard output."
#   action do |args, xargs|
#     print args.join(" ")
#     println unless newline
#   end
# end
# ```
class Crap
  # Shorthand for an array of strings, such as ARGV
  alias Args = Array(String)

  # Shorthand for an `OptionParser#unknown_args` suitable proc.
  alias UnknownArgs = (Args, Args) ->

  # Shorthand for the type of proc which is acceptible as a command action
  alias Action = -> | (Args) -> | (Args, Args) ->

  # Possible return codes from `#parse`
  enum ReturnCode
    OK # No issues
    Test # A test non-success return code
    UnknownError # An unhandled exception was caught
  end

  class Test < Exception
  end

  # Default standard input IO given to all new Craps.
  class_property stdin : IO { STDIN }

  # Default standard output IO given to all new Craps.
  class_property stdout : IO { STDOUT }

  # Default standard error IO given to all new Craps.
  class_property stderr : IO { STDERR }

  # Standard input stream
  getter stdin : IO { self.class.stdin }

  # Standard output stream
  getter stdout : IO { self.class.stdout }

  # Standard error stream
  getter stderr : IO { self.class.stderr }

  # these common methods should be invoked on stdout
  delegate :print, :puts, :printf, to: stdout

  # The banner to set on the underlying OptionParser.
  getter banner : String do
    b = ["Usage:", name]
    b << "[options...]" unless options.empty?
    b << "<command> [args...]" if @action.nil? unless commands.empty?
    b << "[command [args...]]" unless @action.nil? || commands.empty?
    b.compact.join(" ")
  end

  # Gets the OptionParser which is to be used under the hood. The
  # OptionParser is built using the banner method, separators for options and
  # commands, and using all the @options and @commands to add switches.
  getter parser : OptionParser { OptionParser.new }

  # The name of the command. This is used to build the default banner, and as
  # the subcommand-name.
  getter name = PROGRAM_NAME

  # The version of the command. If present, the default set of options will
  # contain a flag to show the version, and the default set of commands will
  # contain a subcommand to show this.
  getter version : String?

  # A short summary shown alongside the name when this is a sub-command
  getter summary : String?

  # A description of what the command does. Shown when the command's help is
  # shown in full.
  getter description : String?

  # A list of options, which are used to create the OptionParser.
  getter options = [] of Option(Bool) | Option(String) | Option(Int32) | Option(Nil)

  # A list of sub-commands, used to create the OptionParser and shown in help.
  # Each command is its own Crap.
  getter commands = [] of Crap

  # An action to be run when no sub-command is provided.
  getter action : Action { ->(args : Args, xargs : Args) { ok(@description) } }

  # A flag which indicates that `setup` has been called (i.e., that the parser
  # had the banner, options and subcommands applied).
  @setup = false

  # Creates a new Crap, with its configuration specified in the block.
  def self.new(*args)
    i = new(*args)
    with i yield
    i
  end

  # Creates a new Crap with the given name, version, summary and description.
  # Options and sub-commands will need to be added later.
  def initialize(name = PROGRAM_NAME, version = nil, summary = nil, description = nil)
    name name
    version version
    summary summary
    description description
  end

  # Sets the name of the command
  def name(@name)
  end

  # Sets the version of the command, and adds an additional option ("-V",
  # "--version") which prints it.
  def version(@version)
    options << Option(Nil).new do
      long "version"
      short 'V'
      summary "prints the version and exits"
      action { stdout.puts @version }
    end unless @version.nil?
  end

  # Sets the command summary.
  def summary(@summary)
  end

  # Sets the command description
  def description(@description)
  end

  # Sets the action to the given block
  macro action(&block)
    action{{block.args.size}} {{block}}
  end

  # Sets the standard input stream
  def stdin(@stdin)
  end

  # Sets the standard output stream
  def stdout(@stdout)
  end

  # Sets the standard error stream
  def stderr(@stderr)
  end

  # Sets the banner, overwriting the default.
  def banner(@banner)
  end

  # Sets the parser to something specific. Do this if you want to
  # override all of the normal behaviour and provide your own underlying
  # implementation. It resets the `@setup` flag.
  def parser(@parser)
    setup(false)
  end
  
  # Adds a subcommand
  def command(name : String, summary : String)
    c = Crap.new do
      name name
      summary summary
    end
    with c yield
    commands << c
  end

  # Adds an option
  macro option(type, value, short, long, summary)
    options << Crap::Option({{type}}).new({{value}}) do
      short {{short}}
      long {{long}}
      summary {{summary}}
    end
  end

  # Adds an option
  macro option(type, value, &)
    o = Crap::Option({{type}}).new({{value}})
    with o yield
    options << o
  end

  # Adds a help option to #options
  def add_help_option!
    options << Option(Nil).new do
      long "help"
      short 'h'
      summary "print help and exit"
      action { stdout.puts parser }
    end
  end

  # Returns an UnknownArgs which does nothing.
  def unknown_args(block : Nil) : UnknownArgs
    ->(args : Args, xargs : Args) { }
  end

  # Returns an UnknownArgs which ignores the args and xargs and simply calls
  # the block.
  def unknown_args(block : ->) : UnknownArgs
    ->(args : Args, xargs : Args) { block.call }
  end

  # Returns an UnknownArgs which ignores the second argument and calls the
  # block with the args.
  def unknown_args(block : (Args) ->) : UnknownArgs
    ->(args : Args, xargs : Args) { block.call(args) }
  end

  # Simply returns the block as is
  def unknown_args(block : UnknownArgs) : UnknownArgs
    block.itself
  end

  # Converts the action into an UnknownArgs, suitable to be used with
  # `OptionParser#unknown_args`.
  def unknown_args
    unknown_args(self.action)
  end

  # Prints the OptionParser
  def to_s
    parser.to_s
  end

  # Sets the `setup` flag explicitely. This is called once by `#setup()`.
  def setup(setup : Bool)
    @setup = setup
  end

  # Returns immediately if the `setup` flag is set; otherwise, calls
  # `#setup(OptionParser)` with `#parser`, and sets the `setup` flag.
  def setup
    return if @setup
    setup(parser)
    setup(true)
  end

  # Sets up an OptionParser with this object's configuration.
  def setup(op : OptionParser)
    op.banner = banner
    op.separator(summary) unless summary.nil?
    op.separator("Options:") unless options.empty?
    options.each { |o| o.apply(op) }
    op.separator(description) unless description.nil?
    op.separator("Commands:") unless commands.empty?
    commands.each { |c| c.apply(op) }
    op.unknown_args &unknown_args
  end

  # Parses the command-line arguments and returns 0 if all is well. It will
  # rescue any raised exceptions, print the message to stderr, and try to match
  # it to a ReturnCode. If it can't find a match, it uses UnknownError.
  def parse(args : Args = ARGV) : ReturnCode
    setup
    parser.parse(args)
    return ReturnCode::OK
  rescue error : Test
    return ReturnCode::Test
  rescue error
    stderr.puts error.message
    return ReturnCode::UnknownError
  end

  # Simply prints out whatever it was passed and returns OK.
  def ok(*args)
    stdout.puts(*args)
  end

  # Adds this command to an OptionParser's flags. The name and summary are used
  # for help, and the command's block sets the OptionParser's `unknown_args` to
  # this Crap's `#unknown_args` (which is built using the `#action`).
  def apply(op : OptionParser)
    op.on(name.to_s, summary.to_s) { op.unknown_args &unknown_args }
  end

  # Makes a new Crap, configures it with the block, and calls its #parse method
  # with the args.
  def self.parse(args = ARGV, &)
    crap = new(PROGRAM_NAME)
    with crap yield
    crap.parse(args)
  end

  private def action0(&@action); end
  private def action1(&@action : Args ->); end
  private def action2(&@action : Args, Args ->); end
end
