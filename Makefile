.PHONY: spec docs

spec:
	crystal spec

docs:
	crystal docs
