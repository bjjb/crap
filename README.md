# crap 💩

A Crystal library to help write command-line applications with programmable
completion, inspired by Rust's [clap][] crate and Go's [cobra][].

## Installation

1. Add the dependency to your `shard.yml`:

   ```yaml
   dependencies:
     crap:
       github: bjjb/crap
   ```

2. Run `shards install`

> The project is developed on GitLab, but mirrored on GitHub.

## Usage

Here's a fairly basic example: a clone of the `echo(1)` command/builtin.

```crystal
require "crap"

Crap.parse do
  name "echo"
  summary "Write arguments to the standard output."
  description <<-DESC
    Display the args, separated by a single space character and followed by a
    newline, on the standard output. Returns success unless a write error
    occurs.
  DESC
  option "-n", "don't append a newline"
  action do |args|
    print args.join(" ")
    puts unless option?("n")
  end
end
```

You can build arbitrarily complex command-line apps by using sub-commands
(Crap#crap). See the [examples][] folder for some inspiration.

## Development

There are no special dependencies; develop it as you would any Crystal
project.

## Related projects

* [cobra][] - a great CLI-helper package in Go
* [clap][] - a great CLI helper package in Rust
* [crapi][] - a compatible Crystal shard to build apps from OpenAPI specs

## Contributing

1. [Fork it](https://gitlab.com/bjjb/crap/-/forks/new)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new merge request

## Contributors

- [JJ Buckley](https://gitlab.com/bjjb) - creator and maintainer

[examples]: https://gitlab.com/bjjb/crap/-/tree/main/examples
[clap]: https://crates.io/crates/clap
[cobra]: https://pkg.go.dev/github.com/spf13/cobra
[crapi]: https://bjjb.gitlab.io/crapi
