require "crap"

Crap.parse do
  summary "Write arguments to the standard output"

  version "0.0.0"
  description <<-DESC
  Displays the ARGs, separated by a new line, on the standard
  output.
  DESC

  newline = true
  options << Crap::Option(Nil).new do
    short 'n'
    summary "Don't print a newline"
    action { newline = false }
  end

  action do |args|
    print args.join(" ")
    puts if newline
  end
end
